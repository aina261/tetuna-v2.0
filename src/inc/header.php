<!DOCTYPE html>
<html lang="en">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122819611-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-122819611-1');
    </script>


    <meta name="Language" content="en"/>
    <meta charset="UTF-8"/>
    <meta name="author" content="Aina"/>
    <meta name="Copyright" content="Tetuna"/>
    <meta name="theme-color" content="#3C3E75"/>
    <meta name="description"
          content="It was in 1998 that Tetuna discovered Psytrance thanks to a track from GMS heard on 'Radio Pulsar',
          a small student radio association. Since then his view of music has changed, and Psy-Trance became part of
          his universe. For five years, he performed as a DJ on various local scenes and since 2003, he has been
          developing his own musical universe, oscillating between full-on, morning and psy-trance, influenced by
          artists such as Bamboo Forest, Sonicsurfeur, Silicon Sound, Toires, Nomad, Neuromotor, Total Eclipse or
          Absolum. He produced his first album Psychedelic Transition in 2009, as well as some EPs with Goa Records. He
          kept on working by producing in 2012, a second album - Crazy Trips - with GeomagneticTV, with which he also
          released his latest album - Alien Existence - in 2016. He performs on stages such as the Openmind Festival
          in Tours (Fr) or the Lunatic Asylum in Lyon (Fr). His creativity and boundless energy make him an
          accomplished artist, ready to make the crowds dance on all dancefloors."/>
    <meta name="keywords"
          content="tetuna, Tetuna, france, full on, progressive, psy, psychedelic trance, psytrance, psytrance france"/>

    <meta property="og:locale" content="en_EN"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="TETUNA"/>
    <meta property="og:description" content="Tetuna, Psytrance artist and live performer"/>
    <meta property="og:image" content="https://tetuna.com/asset/logo/og_logo_black.png"/>
    <meta property="og:url" content="https://tetuna.com"/>
    <meta property="og:site_name" content="tetuna.com"/>

    <meta name="Publisher" content="Tetuna"/>
    <meta name="Revisit-After" content="7 days"/>
    <meta name="robots" content="index"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>

    <title>TETUNA | Artist and live performer</title>

    <link rel="stylesheet" href="css/style.css"/>

    <link rel="icon" type="image/png" href="asset/img/favicon.png"/>

    <link type="text/plain" rel="author" href="https://tetuna.com/humans.txt"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>
<body>

<div id="load">
    <div id="loading">
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
    </div>
</div>